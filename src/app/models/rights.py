# deny => Zugriff nicht erlaubt
# allow => Zugriff erlaubt
# inherit => Zugriff von übergeordnetem Element erben
# forceallow => Allow + Überschreibe untergeordnete elemente mit allow
# forcedeny => Deny + Überschreibe untergeordnete elemente mit deny

from copy import deepcopy
import json
from config import Config
import sqlalchemy
from db.rights import Rights
import threading


class RightsModel():
    users_rights = {}

    def __init__(self, owner):
        self.role_definitions = {}
        self.owner = owner
        self.sem = threading.Semaphore()
        with open('/src/app/ressources/default_rights.json') as json_file:
            self.rule_definitions = json.load(json_file)

        result = Config.db_session.execute(sqlalchemy.text(
            "SELECT name, description, definition FROM rights WHERE (owner='system' or owner = :owner) and name like 'user.%' or name like 'role.%'"),
                                           {'owner': owner})

        for rule in result:
            role_name, role_desc, role_definition = rule
            self.role_definitions[role_name] = json.loads(role_definition)

    def get_role_definition_by_name(self, name):
        return self.role_definitions.get(name)

    def get_members_of_group(self, groupname):
        users_in_group = []
        for rolename, value in self.role_definitions.items():
            #print(rolename, value)
            if groupname in value.get("import",[]):
                users_in_group.append({"username": rolename.replace("user.", "")})
        return users_in_group

    def calculate(self, user_id):
        self.sem.acquire()
        self.users_rights[self.owner + "_" + user_id] = deepcopy(self.rule_definitions)
        self.users_rights[self.owner + "_" + user_id]["states"] = ["initial"]
        result = Config.db_session.query(Rights).filter(sqlalchemy.or_
            (Rights.owner == self.owner, Rights.owner == "system")).filter(Rights.name == user_id).one_or_none()
        Config.db_session.commit()
        user_right_config = {}
        if result is not None:
            user_right_config = json.loads(result.definition)
        else:
            print("Haven't found user rights for user " + user_id + ", owner " + self.owner + ", using default rights")

        try:
            self.solve_imports(user_id, user_right_config)
        except RecursionError as re:
            print("RECURSION ERROR: ", re)
            pass


        self.merge_with_rules(user_id, user_right_config.get("rules"))
        self.users_rights[self.owner + "_" + user_id]["states"] = ["merged"]
        self.solve_inherits(user_id=user_id)
        self.users_rights[self.owner + "_" + user_id]["states"] = ["inherited"]
        self.solve_forced_values(user_id=user_id)
        self.users_rights[self.owner + "_" + user_id]["states"] = ["forced"]
        self.sem.release()
        return self.users_rights[self.owner + "_" + user_id]

    def solve_forced_values(self, user_id):
        users_rights = self.users_rights.get(self.owner + "_" + user_id).get("rules")
        for rule_name, rule_value in users_rights.items():
            if rule_value.get("action", "").startswith("force"):
                for rule_name2 in users_rights.keys():
                    if rule_name2.startswith(rule_name):
                        users_rights[rule_name2]["action"] = rule_value.get("action", "")
        self.users_rights[self.owner + "_" + user_id]["rules"] = users_rights

    def solve_inherits(self, user_id):
        users_rights = self.users_rights.get(self.owner + "_" + user_id).get("rules")
        inherits_exist = True
        while inherits_exist:
            inherits_exist = False
            for rule_name, rule_value in users_rights.items():
                if rule_value.get("action", "") == "inherit":
                    inherits_exist = False
                    users_rights[rule_name] = self.get_parent_value(rule_name, rule_value, users_rights)

        self.users_rights[self.owner + "_" + user_id]["rules"] = users_rights

    def get_parent_value(self, rule_name, rule_value, users_rights):
        rule_parts = rule_name.split(".")
        action = "inherit"
        while action == "inherit" and len(rule_parts) > 1:
            rule_parts = rule_parts[0:-1]
            group_definition = users_rights.get(".".join(rule_parts))
            if group_definition is not None:
                action = group_definition.get("action", "")
                if action != "inherit":
                    rule_value["action"] = action
                    rule_value["weight"] = group_definition.get("weight")
        return rule_value

    def solve_imports(self, user_id, group):
        inherits = group.get("import", [])

        for inherit in inherits:
            parent_rights = deepcopy(self.role_definitions.get(inherit))

            if parent_rights is not None:
                self.merge_with_rules(user_id, parent_rights.get("rules"), source=inherit)
                self.solve_imports(user_id, parent_rights)

    def merge_with_rules(self, user_id, new_rules, source="direct"):
        user_rights_rules = None
        user_rights_rules = self.users_rights.get(self.owner + "_" + user_id, {}).get("rules", {})
        if new_rules is not None:
            for new_rule_key, new_rule_value in new_rules.items():
                new_rule_value["source"] = source
                if user_rights_rules.get(new_rule_key) is None or user_rights_rules.get(new_rule_key).get(
                        "weight") < new_rule_value.get("weight"):
                    user_rights_rules[new_rule_key] = new_rule_value

        self.users_rights[self.owner + "_" + user_id]["rules"] = user_rights_rules

    def get_rights_by_user_id(self, user_id):
        effective_user_rights = self.rule_definitions

        if user_id is not None:
            effective_user_rights = self.calculate(user_id=user_id)
            # effective_user_rights = self.users_rights.get(self.owner + "_" + user_id)
            # if effective_user_rights is None:
            #     print("NOT IN CACHE")
            #     self.calculate(user_id=user_id)
            #     effective_user_rights = self.users_rights.get(self.owner + "_" + user_id)
            # else:
            #     print("FOUND IN CACHE")

            if effective_user_rights is None:
                raise Exception(f"Cannot calculate rights for user_id {user_id}")


        return effective_user_rights

    def get_roles(self, owner):
        db_result = Config.db_session.execute(sqlalchemy.text(
            "SELECT name, description, definition, owner FROM rights WHERE (owner = :owner or owner = 'system') and (name like 'user.%' or name like 'role.%')"),
            {'owner': owner}).all()
        result = {}
        for row in db_result:
            name, description, definition, owner = row
            result[name] = {"description": description, "definition": json.loads(definition),
                            "readonly": True if owner == "system" else False}
        return result

    def get_groups(self, owner):
        db_result = Config.db_session.execute(sqlalchemy.text(
            "SELECT name, description, definition, owner FROM rights WHERE (owner = :owner or owner = 'system') and (name like 'group.%')"),
            {'owner': owner}).all()
        result = {}
        for row in db_result:
            name, description, definition, owner = row
            result[name] = {"description": description, "definition": json.loads(definition),
                            "readonly": True if owner == "system" else False}
        return result

    def get_role_imports_by_user(self, owner, user_id, ignore_owner=False):
        if ignore_owner is True:
            db_result = Config.db_session.execute(sqlalchemy.text(
                "SELECT definition FROM rights WHERE name = :user"),
                {'user': user_id}).one_or_none()
        else:
            db_result = Config.db_session.execute(sqlalchemy.text(
                "SELECT definition FROM rights WHERE owner = :owner and name = :user"),
                {'owner': owner, 'user': user_id}).one_or_none()

        result = []
        if db_result is not None:
            imports, = db_result
            result = json.loads(imports).get("import")

        return result