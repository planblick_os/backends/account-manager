import os
import os.path
import datetime
from M2Crypto import BIO, Rand, SMIME
from email.mime.multipart import MIMEMultipart
from email.mime.base import MIMEBase
from email.mime.text import MIMEText
from email.utils import COMMASPACE, formatdate
from email import encoders
import boto3
import smtplib, ssl

from email.message import Message
from email.mime.text import MIMEText
from email.mime.application import MIMEApplication
from email.mime.multipart import MIMEMultipart

# TODO MOUNT CERTS INTO CONTAINER INSTEAD OF NEEDING THEM IN THE REPOSITORY
signMail = True
if os.path.isfile(os.getenv("MAIL_CERTIFICATES_KEY_PATH")):
    ssl_key = os.getenv("MAIL_CERTIFICATES_KEY_PATH")
else:
    signMail = False
if os.path.isfile(os.getenv("MAIL_CERTIFICATES_CRT_PATH")):
    ssl_cert = os.getenv("MAIL_CERTIFICATES_CRT_PATH")
else:
    signMail = False


sender = 'no-reply@serviceblick.com'
subject = 'Deine Registrierung bei serviceblick.com'
confirmation_mail = '/src/app/ressources/confirmation_mail.txt'
welcome_mail = '/src/app/ressources/welcome_mail.txt'
password_lost_mail = '/src/app/ressources/password_lost_mail.txt'
login_invite_mail = '/src/app/ressources/login_invite_mail.txt'
contact_mail = '/src/app/ressources/contact_email.txt'


def send_contact_mail(payload):
    subject = 'Nachricht aus dem Kontaktformular ' + payload.get("product")
    try:
        to = [os.getenv("CONTACT_MAIL_RECIPIENT")]
        login = os.getenv("SMTP_USERNAME")
        password = os.getenv("SMTP_PASSWORD")

        with open(contact_mail, "r") as cmf:
            body = cmf.read()

        body = body.replace("{NAME}", str(payload.get("name")))
        body = body.replace("{EMAIL}", str(payload.get("email")))
        body = body.replace("{MESSAGE}", str(payload.get("message")))

        msg = MIMEMultipart()

        basemsg = MIMEText(body)
        msg.attach(basemsg)


        if signMail:
            msg_str = msg.as_string()
            buf = BIO.MemoryBuffer(msg_str.encode())

            # load seed file for PRNG
            Rand.load_file('/tmp/randpool.dat', -1)
            smime = SMIME.SMIME()

            # load certificate
            smime.load_key(ssl_key, ssl_cert)

            # sign whole message
            p7 = smime.sign(buf, SMIME.PKCS7_DETACHED)

            # create buffer for final mail and write header
            out = BIO.MemoryBuffer()
            out.write('From: %s\n' % payload.get("email"))
            out.write('To: %s\n' % COMMASPACE.join(to))
            out.write('Date: %s\n' % formatdate(localtime=True))
            out.write('Subject: %s\n' % subject)
            out.write('Auto-Submitted: %s\n' % 'auto-generated')

            # convert message back into string
            buf = BIO.MemoryBuffer(msg_str.encode())

            # append signed message and original message to mail header
            smime.write(out, p7, buf)

            # load save seed file for PRNG
            Rand.save_file('/tmp/randpool.dat')

            content = out.read()
        else:
            msg['Subject'] = subject
            msg['From'] = payload.get("email")
            msg['To'] = COMMASPACE.join(to)
            content = msg.as_string()

        # Log in to server using secure context and send email
        if os.getenv("SMTP_SSL") == "true":
            context = ssl.create_default_context()
            with smtplib.SMTP_SSL(os.getenv("SMTP_HOST"), os.getenv("SMTP_PORT"), context=context) as server:
                server.ehlo()
                if os.getenv("SMTP_AUTH_NEEDED") == "true":
                    server.login(login, password)
                server.sendmail(sender, to, content)
                server.close()
        else:
            with smtplib.SMTP(os.getenv("SMTP_HOST"), os.getenv("SMTP_PORT")) as server:
                server.ehlo()
                if os.getenv("SMTP_AUTH_NEEDED") == "true":
                    server.login(login, password)
                server.sendmail(sender, to, content)
                server.close()


        print('Contact mail sent', payload)
        return True
    except Exception as e:
        print(e)
        import traceback
        print(traceback.format_exc())
        return False

def send_login_invitation_mail(recipient, temporary_api_key):
    subject = 'Jemand hat Dich eingeladen'
    try:
        if isinstance(recipient, str):
            to = [recipient]
        login = os.getenv("SMTP_USERNAME")
        password = os.getenv("SMTP_PASSWORD")
        basedomain = os.getenv("SYSTEM_FRONTEND_BASEDOMAIN")
        reset_link = f"{basedomain}/registration/login_invite.html?k=" + str(temporary_api_key) + "&u=" + str(recipient)
        #body = 'Bitte rufe folgenden Link auf um Deine Registrierung bei easy2track abzuschliessen: ' + str(confirmation_link)

        with open(login_invite_mail, "r") as cmf:
            body = cmf.read()

        body = body.replace("{LINK}", str(reset_link))

        msg = MIMEMultipart()

        basemsg = MIMEText(body)
        msg.attach(basemsg)


        if signMail:
            msg_str = msg.as_string()
            buf = BIO.MemoryBuffer(msg_str.encode())

            # load seed file for PRNG
            Rand.load_file('/tmp/randpool.dat', -1)
            smime = SMIME.SMIME()

            # load certificate
            smime.load_key(ssl_key, ssl_cert)

            # sign whole message
            p7 = smime.sign(buf, SMIME.PKCS7_DETACHED)

            # create buffer for final mail and write header
            out = BIO.MemoryBuffer()
            out.write('From: %s\n' % sender)
            out.write('To: %s\n' % COMMASPACE.join(to))
            out.write('Date: %s\n' % formatdate(localtime=True))
            out.write('Subject: %s\n' % subject)
            out.write('Auto-Submitted: %s\n' % 'auto-generated')

            # convert message back into string
            buf = BIO.MemoryBuffer(msg_str.encode())

            # append signed message and original message to mail header
            smime.write(out, p7, buf)

            # load save seed file for PRNG
            Rand.save_file('/tmp/randpool.dat')

            content = out.read()
        else:
            msg['Subject'] = subject
            msg['From'] = sender
            msg['To'] = COMMASPACE.join(to)
            content = msg.as_string()


        # Log in to server using secure context and send email
        if os.getenv("SMTP_SSL") == "true":
            context = ssl.create_default_context()
            with smtplib.SMTP_SSL(os.getenv("SMTP_HOST"), os.getenv("SMTP_PORT"), context=context) as server:
                server.ehlo()
                if os.getenv("SMTP_AUTH_NEEDED") == "true":
                    server.login(login, password)
                server.sendmail(sender, to, content)
                server.close()
        else:
            with smtplib.SMTP(os.getenv("SMTP_HOST"), os.getenv("SMTP_PORT")) as server:
                server.ehlo()
                if os.getenv("SMTP_AUTH_NEEDED") == "true":
                    server.login(login, password)
                server.sendmail(sender, to, content)
                server.close()

        print('Password lost email sent to ', recipient)
        return True
    except Exception as e:
        print(e)
        import traceback
        print(traceback.format_exc())
        return False


def send_password_lost_mail(recipient, temporary_api_key, user_login):

    subject = 'Passwort vergessen bei planBLICK'
    try:
        if isinstance(recipient, str):
            to = [recipient]
        login = os.getenv("SMTP_USERNAME")
        password = os.getenv("SMTP_PASSWORD")
        basedomain = os.getenv("SYSTEM_FRONTEND_BASEDOMAIN")
        reset_link = f"{basedomain}/login/password_lost.html?k=" + str(temporary_api_key) + "&u=" + str(user_login)
        #body = 'Bitte rufe folgenden Link auf um Deine Registrierung bei easy2track abzuschliessen: ' + str(confirmation_link)

        with open(password_lost_mail, "r") as cmf:
            body = cmf.read()

        body = body.replace("{LINK}", str(reset_link))

        msg = MIMEMultipart()

        basemsg = MIMEText(body)
        msg.attach(basemsg)


        if signMail:
            msg_str = msg.as_string()
            buf = BIO.MemoryBuffer(msg_str.encode())

            # load seed file for PRNG
            Rand.load_file('/tmp/randpool.dat', -1)
            smime = SMIME.SMIME()

            # load certificate
            smime.load_key(ssl_key, ssl_cert)

            # sign whole message
            p7 = smime.sign(buf, SMIME.PKCS7_DETACHED)

            # create buffer for final mail and write header
            out = BIO.MemoryBuffer()
            out.write('From: %s\n' % sender)
            out.write('To: %s\n' % COMMASPACE.join(to))
            out.write('Date: %s\n' % formatdate(localtime=True))
            out.write('Subject: %s\n' % subject)
            out.write('Auto-Submitted: %s\n' % 'auto-generated')

            # convert message back into string
            buf = BIO.MemoryBuffer(msg_str.encode())

            # append signed message and original message to mail header
            smime.write(out, p7, buf)

            # load save seed file for PRNG
            Rand.save_file('/tmp/randpool.dat')

            content = out.read()
        else:
            msg['Subject'] = subject
            msg['From'] = sender
            msg['To'] = COMMASPACE.join(to)
            content = msg.as_string()


        # Log in to server using secure context and send email
        if os.getenv("SMTP_SSL") == "true":
            context = ssl.create_default_context()
            with smtplib.SMTP_SSL(os.getenv("SMTP_HOST"), os.getenv("SMTP_PORT"), context=context) as server:
                server.ehlo()
                if os.getenv("SMTP_AUTH_NEEDED") == "true":
                    server.login(login, password)
                server.sendmail(sender, to, content)
                server.close()
        else:
            with smtplib.SMTP(os.getenv("SMTP_HOST"), os.getenv("SMTP_PORT")) as server:
                server.ehlo()
                if os.getenv("SMTP_AUTH_NEEDED") == "true":
                    server.login(login, password)
                server.sendmail(sender, to, content)
                server.close()

        print('Password lost email sent to ', recipient)
        return True
    except Exception as e:
        print(e)
        import traceback
        print(traceback.format_exc())
        return False


def send_registration_mail(recipient, registration_key):
    print("SIGNMAIL", signMail)
    subject = 'Deine Registrierung bei planBLICK'
    try:
        if isinstance(recipient, str):
            to = [recipient]
        login = os.getenv("SMTP_USERNAME")
        password = os.getenv("SMTP_PASSWORD")
        basedomain = os.getenv("SYSTEM_FRONTEND_BASEDOMAIN")
        confirmation_link = f"{basedomain}/registration/confirm.html?k=" + str(registration_key)
        #body = 'Bitte rufe folgenden Link auf um Deine Registrierung bei easy2track abzuschliessen: ' + str(confirmation_link)

        with open(confirmation_mail, "r") as cmf:
            body = cmf.read()

        body = body.replace("{LINK}", str(confirmation_link))

        msg = MIMEMultipart()

        basemsg = MIMEText(body)
        msg.attach(basemsg)


        if signMail:
            msg_str = msg.as_string()
            buf = BIO.MemoryBuffer(msg_str.encode())

            # load seed file for PRNG
            Rand.load_file('/tmp/randpool.dat', -1)
            smime = SMIME.SMIME()

            # load certificate
            smime.load_key(ssl_key, ssl_cert)

            # sign whole message
            p7 = smime.sign(buf, SMIME.PKCS7_DETACHED)

            # create buffer for final mail and write header
            out = BIO.MemoryBuffer()
            out.write('From: %s\n' % sender)
            out.write('To: %s\n' % COMMASPACE.join(to))
            out.write('Date: %s\n' % formatdate(localtime=True))
            out.write('Subject: %s\n' % subject)
            out.write('Auto-Submitted: %s\n' % 'auto-generated')

            # convert message back into string
            buf = BIO.MemoryBuffer(msg_str.encode())

            # append signed message and original message to mail header
            smime.write(out, p7, buf)

            # load save seed file for PRNG
            Rand.save_file('/tmp/randpool.dat')

            content = out.read()
        else:
            msg['Subject'] = subject
            msg['From'] = sender
            msg['To'] = COMMASPACE.join(to)
            content = msg.as_string()


        # Log in to server using secure context and send email
        if os.getenv("SMTP_SSL") == "true":
            context = ssl.create_default_context()
            with smtplib.SMTP_SSL(os.getenv("SMTP_HOST"), os.getenv("SMTP_PORT"), context=context) as server:
                server.ehlo()
                if os.getenv("SMTP_AUTH_NEEDED") == "true":
                    print("CREDENTIALS", login, password)
                    server.login(login, password)
                server.sendmail(sender, to, content)
                server.close()
        else:
            with smtplib.SMTP(os.getenv("SMTP_HOST"), os.getenv("SMTP_PORT")) as server:
                server.ehlo()
                if os.getenv("SMTP_AUTH_NEEDED") == "true":
                    server.login(login, password)
                server.sendmail(sender, to, content)
                server.close()

        print('Registration-confirmation-email sent to ', recipient)
    except Exception as e:
        print(e)
        import traceback
        print(traceback.format_exc())



def send_welcome_mail(recipient):
    try:
        if isinstance(recipient, str):
            to = [recipient]
        login = os.getenv("SMTP_USERNAME")
        password = os.getenv("SMTP_PASSWORD")

        subject = 'Deine Registrierung bei planBLICK ist jetzt abgeschlossen'
        body = 'Vielen Dank für Deine Registrierung. Du kannst Dich jetzt mit Deiner Email und Deinem Passwort anmelden'

        with open(welcome_mail, "r") as cmf:
            body = cmf.read()

        msg = MIMEMultipart()

        basemsg = MIMEText(body)
        msg.attach(basemsg)


        if signMail:
            msg_str = msg.as_string()
            buf = BIO.MemoryBuffer(msg_str.encode())

            # load seed file for PRNG
            Rand.load_file('/tmp/randpool.dat', -1)
            smime = SMIME.SMIME()

            # load certificate
            smime.load_key(ssl_key, ssl_cert)

            # sign whole message
            p7 = smime.sign(buf, SMIME.PKCS7_DETACHED)

            # create buffer for final mail and write header
            out = BIO.MemoryBuffer()
            out.write('From: %s\n' % sender)
            out.write('To: %s\n' % COMMASPACE.join(to))
            out.write('Date: %s\n' % formatdate(localtime=True))
            out.write('Subject: %s\n' % subject)
            out.write('Auto-Submitted: %s\n' % 'auto-generated')

            # convert message back into string
            buf = BIO.MemoryBuffer(msg_str.encode())

            # append signed message and original message to mail header
            smime.write(out, p7, buf)

            # load save seed file for PRNG
            Rand.save_file('/tmp/randpool.dat')

            content = out.read()
        else:
            msg['Subject'] = subject
            msg['From'] = sender
            msg['To'] = COMMASPACE.join(to)
            content = msg.as_string()
            

        # Log in to server using secure context and send email
        if os.getenv("SMTP_SSL") == "true":
            context = ssl.create_default_context()
            with smtplib.SMTP_SSL(os.getenv("SMTP_HOST"), os.getenv("SMTP_PORT"), context=context) as server:
                server.ehlo()
                if os.getenv("SMTP_AUTH_NEEDED") == "true":
                    server.login(login, password)
                server.sendmail(sender, to, content)
                server.close()
        else:
            with smtplib.SMTP(os.getenv("SMTP_HOST"), os.getenv("SMTP_PORT")) as server:
                server.ehlo()
                if os.getenv("SMTP_AUTH_NEEDED") == "true":
                    server.login(login, password)
                server.sendmail(sender, to, content)
                server.close()


        print('Registration-confirmation-email sent to ', recipient)
    except Exception as e:
        print(e)
        import traceback
        print(traceback.format_exc())


