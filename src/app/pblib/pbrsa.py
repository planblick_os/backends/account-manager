import base64
import os
import rsa
import binascii
import requests
import json

from config import Config, singleton

from pathlib import Path

@singleton
class Pbrsa():
    def __init__(self):
        self.private_key = None
        self.public_key = None
        self.fetch_keys_from_certificate_store(Config.service_name)

    def fetch_keys_from_certificate_store(self, service_name):
        url = f"{os.getenv('CERTIFICATE_STORE_BASE_URL')}/get_legacy_certificate?service_name={service_name}"
        print("URL", url)
        payload = ""
        headers = {}

        response = requests.request("GET", url, headers=headers, data=payload)
        json = response.json()

        self.private_key = json.get("id_rsa")
        self.public_key = json.get("id_rsa.pem")

        return json

    def load_priv_key(self):
        return rsa.PrivateKey.load_pkcs1(self.private_key)

    def load_pub_key(self):
        return rsa.PublicKey.load_pkcs1(self.public_key)

    def verify(self, message, signature, pubkey=None):
        if pubkey is None:
            pubkey = load_pub_key()
        signature = base64.b64decode(signature)
        return rsa.verify(message, signature, pubkey)

    def sign(self, message, privkey=None):
        if privkey is None:
            privkey = load_priv_key()
        signature = rsa.sign(message, privkey, 'SHA-1')
        return base64.b64encode(signature)

    def encrypt(self, string):
        if type(string) == str:
            string = string.encode('utf-8')
        entropy_pwd = base64.b64encode(rsa.encrypt(string, load_pub_key()))
        return entropy_pwd.decode()

    def decrypt(self, crypted):
        if type(crypted) == str:
            crypted = crypted.encode()
        crypted = base64.b64decode(crypted)
        return rsa.decrypt(crypted, load_priv_key()).decode("utf-8")


def load_priv_key(path=None):
    return Pbrsa().load_priv_key()

def load_pub_key(path=None):
    return Pbrsa().load_pub_key()

def verify(message, signature, pubkey=None):
    return Pbrsa().verify(message, signature, pubkey)

def sign(message, privkey=None):
    return Pbrsa().sign(message, privkey)

def encrypt(string):
    return Pbrsa().encrypt(string)

def decrypt(crypted):
    return Pbrsa().decrypt(crypted)
