from db.abstract import Migrate
from sqlalchemy import types
from db.account import Account
from db.logins import Logins
import json

def migrate_db():
    print("CREATING TABLE ACCOUNTS FOR ACCOUNT-MANAGER IF NOT EXISTS")
    Account()
    print("CREATING TABLE LOGINS FOR ACCOUNT-MANAGER IF NOT EXISTS")
    Logins()
    migration = Migrate()

    # 03.12.2019
    migration.add_column(table_name="accounts", column_name="login", data_type=types.VARCHAR(255))
    migration.execute("Set login to email", 'UPDATE accounts set login = email where login IS NULL')
    migration.add_column(table_name="accounts", column_name="profile_data", data_type=types.Text())

    default_profile_data = json.dumps({
        "email": "",
        "lastname": "",
        "firstname": ""
    })

    migration.execute("Set login to email", f'UPDATE accounts set profile_data = :default_profile_data where profile_data IS NULL', {"default_profile_data": default_profile_data})

    migration.add_column(table_name="rights", column_name="data_owners", data_type=types.TEXT())
    default_data_owners = '["role.sys_admin"]'
    migration.execute("Set default data_owners", f"UPDATE `rights` set data_owners = '{default_data_owners}' where data_owners is null;")