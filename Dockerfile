FROM registry.gitlab.com/crowdsoft-foundation/various/python-base-image:production

# install pip modules (own filesystem layer)
COPY src/requirements.txt /src/requirements.txt
RUN pip3 install -r /src/requirements.txt --no-cache-dir

# copy our app
COPY src /src


# some info for the callers
EXPOSE 8000
#VOLUME ["/src"]
WORKDIR "/src"

# run our service
RUN chmod 777 /src/runApp.sh && chmod +x /src/runApp.sh
CMD ["/src/runApp.sh"]


